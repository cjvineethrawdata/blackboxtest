package com.halo.tests;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.halo.contant.Constants;
import com.halo.objects.Device;
import com.halo.objects.LoginPageObjects;
import com.halo.pages.LoginPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseClass {

	private String reportDirectory = "reports";
	private String reportFormat = "xml";
	private String testName = "Untitled";
	public AndroidDriver<MobileElement> driver = null;
	public LoginPage login;
	DesiredCapabilities dc;
	public LoginPageObjects loginPageObjects;
	List<Device> devices;
	 WebDriverWait wait ;
	 
	@BeforeTest
	public void setup() {
		initCapabilities();
		initObjects();
		initPages();
		
	}

	private void initObjects() {

		loginPageObjects = new LoginPageObjects();
		PageFactory.initElements(new AppiumFieldDecorator(driver), loginPageObjects);
		wait= new WebDriverWait(driver, 100);

	}

	private void initPages() {

		login = new LoginPage(driver, dc, loginPageObjects);

	}

	private void initCapabilities() {
		try {

			devices = getDevices();
			dc = new DesiredCapabilities();
			dc.setCapability("reportDirectory", reportDirectory);
			dc.setCapability("reportFormat", reportFormat);
			dc.setCapability("testName", testName);
			dc.setCapability(MobileCapabilityType.PLATFORM_NAME, devices.get(0).getPlatformName());
			dc.setCapability(MobileCapabilityType.UDID, devices.get(0).getUdid());
			dc.setCapability(MobileCapabilityType.DEVICE_NAME, devices.get(0).getDeviceName());
			dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, devices.get(0).getPlatformVersion());
			dc.setCapability(MobileCapabilityType.APP, GenerateResourcePath("apk_app-cognitive-comcast.apk"));
			dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, devices.get(0).getAppPackage());
			dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, devices.get(0).getAppActivity());
			dc.setCapability("autoAcceptAlerts", true);
			driver = new AndroidDriver<MobileElement>(new URL(Constants.APPIUM_SERVER), dc);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.print(e.getMessage());
		}

	}

	@AfterTest
	public void teardown() {

		driver.quit();

	}

	String GenerateResourcePath(String resouseName) {

		URL resource = getClass().getClassLoader().getResource(Constants.RESOURCE_FOLDER_PATH_ANDROID + resouseName);
		return resource.toString().replace("file:/", "").replace("/", "//");
	}

	public List<Device> getDevices() {

		List<Device> deviceList = new ArrayList<Device>();
		String deviceFile = "devices/devices.json";
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(deviceFile);
		InputStreamReader streamReader = new InputStreamReader(inputStream);
		JSONObject jsonObject = new JSONObject(new JSONTokener(streamReader));

		System.out.print(jsonObject.toString());
		JSONArray jsonArray = jsonObject.getJSONArray("devices");
		for (int i = 0; i < jsonArray.length(); i++) {
			Device device = new Device();

			device.setAppActivity(jsonArray.getJSONObject(i).optString("appActivity"));
			device.setAppPackage(jsonArray.getJSONObject(i).optString("appPackage"));
			device.setPlatformName(jsonArray.getJSONObject(i).optString("platformName"));
			device.setPlatformVersion(jsonArray.getJSONObject(i).optString("platformVersion"));
			device.setUdid(jsonArray.getJSONObject(i).optString("udid"));
			device.setDeviceName(jsonArray.getJSONObject(i).optString("deviceName"));
			deviceList.add(device);

		}
		return deviceList;
	}
	
	public ExpectedCondition<Boolean> elementFoundAndClicked(final By locator) {
	    return new ExpectedCondition<Boolean>() {
	      
        public Boolean apply(WebDriver input) {
				// TODO Auto-generated method stub
				 WebElement el = driver.findElement(locator);
		            el.click();
		            return true;
			}
	    };
	}
}
