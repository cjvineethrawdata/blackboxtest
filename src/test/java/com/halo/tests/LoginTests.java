package com.halo.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.halo.contant.AppStrings;
import com.halo.contant.Locator;
import com.halo.utils.Utils;


public class LoginTests extends BaseClass{
	
	
   @Test(priority = 1)
	public void testLoginWithInvalidCredentials() throws InterruptedException {	
	login.login(AppStrings.USER_EMAIL, AppStrings.INVALID_PASSWORD);
	Utils.WaitUntilViewDiaplay(driver,10, Locator.ID,"cognitive.com.halo:id/error_text");
	assertEquals(loginPageObjects.errorMessage.getText(),AppStrings.INVALID_CREDENTIAL_MESSAGE);
	}
	
	

	
	@Test(priority = 2)
	public void testLoginWithValidUser() throws InterruptedException {		
		
		login.login(AppStrings.USER_EMAIL, AppStrings.USER_PASSWORD);
	   wait.until(elementFoundAndClicked(By.id("cognitive.com.halo:id/largeLabel")));
	assertEquals(driver.findElement(By.id("cognitive.com.halo:id/largeLabel")).getText(),AppStrings.HOME_STRING);
	
	}
	

}
