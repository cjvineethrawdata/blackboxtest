package com.halo.contant;

public class Constants {

	public static final String APPIUM_SERVER="http://127.0.0.1:4723/wd/hub";
	public static final String RESOURCE_FOLDER_PATH_ANDROID="apps/apk/";
	public static final String APP_PACKAGE="cognitive.com.halo";
	
}
