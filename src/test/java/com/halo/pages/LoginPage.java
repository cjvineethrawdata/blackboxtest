package com.halo.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.halo.objects.LoginPageObjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LoginPage {
	
	DesiredCapabilities dc;
	LoginPageObjects loginPageObjects;
	AndroidDriver<MobileElement> driver;
	
	public LoginPage(AndroidDriver<MobileElement> driver,DesiredCapabilities dc,LoginPageObjects loginPageObjects){
		this.dc=dc;
		this.driver=driver;
	    this.loginPageObjects=loginPageObjects;
	}

	
public  void login(String username ,String password) throws InterruptedException {
	Thread.sleep(1000);
	if (driver.findElements(By.id("com.android.packageinstaller:id/permission_allow_button")).size() > 0) {
       
		driver.findElement(By.id(
		 "com.android.packageinstaller:id/permission_allow_button")).click();
    } else {
        System.out.println("NOT FOUND!");
    }

	
	 loginPageObjects.loginEmail.sendKeys(username);
	 loginPageObjects.loginPassword.sendKeys(password);
	 loginPageObjects.btnLogin.click();	
	 Thread.sleep(3000);
}
		
}
