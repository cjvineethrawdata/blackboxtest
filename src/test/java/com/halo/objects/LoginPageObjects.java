package com.halo.objects;


import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPageObjects {

    @CacheLookup
    @AndroidFindBy(id = "cognitive.com.halo:id/et_email")
    @FindBy(id = "et_email") public MobileElement loginEmail;
    
    @CacheLookup
    @AndroidFindBy(id = "cognitive.com.halo:id/et_password")
    @FindBy(id = "et_password") public MobileElement loginPassword ;
    
    @CacheLookup
    @AndroidFindBy(id = "cognitive.com.halo:id/btn_login")
    @FindBy(id = "btn_login") public MobileElement btnLogin;
    
    @CacheLookup
    @AndroidFindBy(id = "cognitive.com.halo:id/error_text")
    @FindBy(id = "error_text") public MobileElement errorMessage;
}
