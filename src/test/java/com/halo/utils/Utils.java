package com.halo.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.halo.contant.Locator;

public class Utils {


	public static void WaitUntilViewDiaplay(WebDriver driver,long waitTime,Locator locator,String locator_str) {
		switch (locator) {
		case ID:
			new WebDriverWait(driver, waitTime).until(ExpectedConditions.presenceOfElementLocated(By.id(locator_str)));
			break;
		case CLASS:
			new WebDriverWait(driver, waitTime).until(ExpectedConditions.presenceOfElementLocated(By.className(locator_str)));
			break;
			
		case XPATH:
			new WebDriverWait(driver, waitTime).until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator_str)));
			break;
			
		case TAG_NAME:
			new WebDriverWait(driver, waitTime).until(ExpectedConditions.presenceOfElementLocated(By.tagName(locator_str)));
			break;

		default:
			break;
		}

	}
}
